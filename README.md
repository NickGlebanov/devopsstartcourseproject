## Схема текущей инфраструктуры
![схема](docs/images/infra.png)


# 0. Подготовка окружения

## Готовим инфраструктуру

* Установить Vagrant
```shell
sudo apt update
sudo apt install virtualbox
curl -O https://releases.hashicorp.com/vagrant/2.2.9/vagrant_2.2.9_x86_64.deb
sudo apt install ./vagrant_2.2.9_x86_64.deb
vagrant --version
```

* Подготовить образ VM или взять готовый https://www.osboxes.org/debian/#debian-11-vbox
* Запустить терминал в папке /mnt/c/Program Files/Oracle/VirtualBox
![img_1.png](docs/images/img_1.png)
* Экспортируем подготовленный пустой образ в формат Vagrant
```
vagrant package --base adbd6606-766f-4d62-be07-4bc8263865ec --output ~/d11defaultbox.box
vagrant box add deb11stub ~/d11defaultbox.box
```
* для WSL также -> ``vagrant plugin install virtualbox_WSL2``

### Заметки
* Убить все процессы вагрант ``ps -ef | grep 'vagrant' | grep -v grep | awk '{print $2}' | xargs -r kill -9``
* Запуск машины ``vagrant up``
* Остановка машины ``vagrant down``
* Подключиться к машине ``vagrant ssh``
* Уничтожить машину ``vagrant destroy``
* Проверить все хосты в локальной сети ``sudo nmap -sn 192.168.3.*``

# 1. Развёртывание удостоверяющего центра для выдачи сертификатов (Public Key Infrastructure)

## Установка центра сертификации (ca_vagrant_vm)
* Скачать готовый пакет из репозитория https://gitlab.com/devops-start-skb/center-authority-package , файл ``ca-my-org.deb``
* В папке ./Certification_..... `vagrant up` и установить пакет ``sudo dpkg -i /home/user/ca-my-org.deb``
* Cгенерировать запросы для клиента и подписать их

# 2. Создание и настройка VPN-сервера

## Установка vpn сервера (vpn_vagrant_vm)
* ``vagrant up`` из папки
* ``vagrant ssh`` и Выполнить 

## Установка клиента openvpn
* https://openvpn.net/client-connect-vpn-for-windows/
* Скачать конфиг можно командой ``scp -r nngle@192.168.3.52:~/clntconfig ~/clntconfig``

### Заметки
* Связанные команды cmd (windows)
  - вывести все процессы с частью имени равным vpn ``tasklist | findstr "vpn"``
  - убить процесс ``taskkill /F /PID 26028``
  - запустить openvpn не меняя systemd после перезагрузки машины ``sudo /usr/sbin/openvpn --config /etc/openvpn/server/server.conf --status /var/log/openvpn/ovpn.status --ca /home/nngle/ovpn-certs/ca.crt --cert /home/nngle/ovpn-certs/server.crt --key /home/nngle/ovpn-certs/server.key``

# 3. Настройка мониторинга

## Установить OpenVPN экспортер для prometheus на vpn сервере
* Установить докер https://docs.docker.com/engine/install/debian/
* запустить экспортер OpenVpn
```shell
sudo docker run -d -p 9176:9176 \
  -v /var/log/openvpn/ovpn.status:/etc/openvpn_exporter/server.status \
  kumina/openvpn-exporter -openvpn.status_paths /etc/openvpn_exporter/server.status
```
## Установить NodeExporter на CA и VPN сервера (192.168.3.51 и 192.168.3.52)
```shell
wget https://github.com/prometheus/node_exporter/releases/download/v1.5.0/node_exporter-1.5.0.linux-amd64.tar.gz
tar xvfz node_exporter-1.5.0.linux-amd64.tar.gz
cd node_exporter-1.5.0.linux-amd64
./node_exporter
```
![img.png](docs/images/img.png)
## Запустить машину с мониторингом (``cd monitoring``)
* ``vagrant up``
### Скриншоты запущенных систем и минимальный пример дашборда
![img.png](docs/images/img_graf.png)
![img.png](docs/images/img_prom_al.png)


> # Далее
> 1. [План хранения и восстановления](docs/backup_service/BACKUP_PLAN.md)
> 2. [Сценарии отказа и реагирование](docs/backup_service/FAILURE_SCENARIOS.md)