#!/usr/bin/env bash

sudo scp -r nngle@192.168.3.51:/home/nngle/ovpn-certs/ /home/nngle
sudo cp -r /home/nngle/ovpn-certs/** /etc/openvpn/server
sudo /usr/sbin/openvpn --genkey --secret /home/nngle/ta.key
sudo cp /home/nngle/ta.key /etc/openvpn/server/

sudo chmod 777 /home/nngle/ovpn-certs/



sudo cp /home/nngle/ovpn-certs/client-1.crt /etc/openvpn/client/
sudo cp /home/nngle/openvpn-configuration/openvpn-server.conf /etc/openvpn/server/server.conf
sudo /usr/sbin/openvpn --config /etc/openvpn/server/server.conf --status /var/log/openvpn/ovpn.status --ca /home/nngle/ovpn-certs/ca.crt --cert /home/nngle/ovpn-certs/server.crt --key /home/nngle/ovpn-certs/server.key
sudo mkdir /home/nngle/clntconfig
sudo cp /home/nngle/ta.key /home/nngle/clntconfig
sudo bash /home/nngle/scripts/make_config.sh client-1

sudo chown -R nngle:nngle ovpnconf/